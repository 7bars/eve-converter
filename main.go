package main

import (
	"bytes"
	"compress/zlib"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	name := []string{"nvdcve-1.0-2002.json",
		"nvdcve-1.0-2003.json",
		"nvdcve-1.0-2004.json",
		"nvdcve-1.0-2005.json",
		"nvdcve-1.0-2006.json",
		"nvdcve-1.0-2007.json",
		"nvdcve-1.0-2008.json",
		"nvdcve-1.0-2009.json",
		"nvdcve-1.0-2011.json",
		"nvdcve-1.0-2012.json",
		"nvdcve-1.0-2013.json",
		"nvdcve-1.0-2014.json",
		"nvdcve-1.0-2015.json",
		"nvdcve-1.0-2016.json",
		"nvdcve-1.0-2017.json",
		"nvdcve-1.0-2018.json"}
	k := 0
	for i := range name {
		dat, err := ioutil.ReadFile(name[i])
		if err != nil {
			fmt.Println(err)
		}
		var tmp CVE
		err = json.Unmarshal(dat, &tmp)
		if err != nil {
			fmt.Println(err)
		}

		for j := range tmp.CVEItems {
			b, err := json.Marshal(&tmp.CVEItems[j])
			if err != nil {
				fmt.Println(err)
			} else {
				setupCVE(b, fmt.Sprint(k))
				fmt.Println("file: ", fmt.Sprintln(i), "number: ", fmt.Sprintln(k))
				k++
			}

		}
	}
	// dat, err := ioutil.ReadFile("example.json")
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// var tmp CVE
	// err = json.Unmarshal(dat, &tmp)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// for i := range tmp.CVEItems {
	// 	b, err := json.Marshal(&tmp.CVEItems[i])
	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}
	// 	setupCVE(b, fmt.Sprint(i))
	// }
	// var tmp CVE
	// err := downloadCVE("https://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-recent.json.gz", tmp)
	// if err != nil {
	// 	fmt.Println(err)
	// } else {

	// 	// err = json.Unmarshal(data, &tmp)
	// 	// if err != nil {
	// 	// 	fmt.Println(err)
	// 	// }
	// 	for i := range tmp.CVEItems {
	// 		b, err := json.Marshal(&tmp.CVEItems[i])
	// 		if err != nil {
	// 			fmt.Println(err)
	// 		} else {
	// 			setupCVE(b, fmt.Sprint(i))
	// 			fmt.Println("Index:", fmt.Sprintln(i), " was set.")
	// 		}
	// 	}
	// }
}

//setupCVE() add doc "name" type of "cve" index of "cve" to Elasticsearch
func setupCVE(jsonStr []byte, name string) {
	url := "http://127.0.0.1:9200/cve/cve/" + name
	// fmt.Println("URL:>", url)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	// fmt.Println("response Status:", resp.Status)
	// fmt.Println("response Headers:", resp.Header)
	// body, _ := ioutil.ReadAll(resp.Body)
	// fmt.Println("response Body:", string(body))
}

//downloadCVE from definition url and return unzip
func downloadCVE(url string, target interface{}) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	// defer resp.Body.Close()
	z, err := zlib.NewReader(resp.Body)
	if err != nil {
		return err
	}
	resp.Body.Close()

	p, err := ioutil.ReadAll(z)
	if err != nil {
		return err
	}
	// return json.NewDecoder(resp.Body).Decode(target)
	return json.Unmarshal(p, target)
	// err = ioutil.WriteFile("1.json", p, 0644)
	// if err != nil {
	// 	log.Panicln(err)
	// }
	// return p, err
}
